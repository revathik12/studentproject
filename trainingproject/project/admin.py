from django.contrib import admin
from project.models import *

# Register your models here.
admin.site.register(Subject)
admin.site.register(Grade)
admin.site.register(Section)
admin.site.register(student)