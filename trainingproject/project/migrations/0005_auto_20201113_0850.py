# Generated by Django 2.2.12 on 2020-11-13 08:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0004_student_sec'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='subject',
            name='subject1',
        ),
        migrations.RemoveField(
            model_name='subject',
            name='subject2',
        ),
        migrations.RemoveField(
            model_name='subject',
            name='subject3',
        ),
        migrations.RemoveField(
            model_name='subject',
            name='subject4',
        ),
        migrations.RemoveField(
            model_name='subject',
            name='subject5',
        ),
        migrations.RemoveField(
            model_name='subject',
            name='subject6',
        ),
        migrations.AddField(
            model_name='subject',
            name='subject',
            field=models.CharField(default='', max_length=15),
        ),
    ]
