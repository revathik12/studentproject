from django.db import models

class Subject(models.Model):
    subid= models.AutoField(primary_key=True)
    subject=models.CharField(max_length=15,default='')
    def __str__(self):
        return self.subject

class Grade(models.Model):
    gradeName =models.CharField(max_length=3)
    gid= models.AutoField(primary_key=True)
    subjects= models.ForeignKey(Subject,on_delete=models.deletion.CASCADE)

    def __str__(self):
        return self.gradeName


class Section(models.Model):
    secid= models.AutoField(primary_key=True)
    section=models.CharField(max_length=1)
    grade= models.ForeignKey(Grade,on_delete=models.deletion.CASCADE)
    subjects= models.ForeignKey(Subject,on_delete=models.deletion.CASCADE,default='')

    
    def __str__(self):
        return self.section
            
class student(models.Model):
    name= models.CharField(max_length=25)
    sid= models.AutoField(primary_key=True)
    grade= models.ForeignKey(Grade,on_delete=models.deletion.CASCADE)
    sec=  models.CharField(max_length=1,default='')
    email= models.EmailField(max_length=30)
    phone_number= models.IntegerField()
    profile_pic = models.ImageField(upload_to=None, height_field=None, width_field=None, max_length=100)

    def __str__(self):
        return self.name