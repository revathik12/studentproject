from django.shortcuts import render
from . import forms
from .models import Section

# Create your views here.
def home(request):
    return render(request,"main_portal.html")

def academics(request):
    form=forms.GradeForm
    if request.method=='POST':
        form=forms.GradeForm(request.POST)
        if form.is_valid():
            form.save(commit=True)

    x= Section.objects.all()
    print(x)
    
    return render(request,"Academics.html",{'form':form,'sec':x})