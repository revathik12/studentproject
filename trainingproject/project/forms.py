from django import forms
from project.models import Section,student

class GradeForm(forms.ModelForm):
    class Meta:
        model=Section
        exclude=['secid']


class StudentForm(forms.ModelForm):
    class Meta:
        model=student
        exclude=['sid']

