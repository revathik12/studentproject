from django.db import models

# Create your models here.
class sections(models.Model):
    secid= models.AutoField(primary_key=True)
    section=models.CharField(max_length=1)

class subjects(models.Model):
    subid= models.AutoField(primary_key=True)
    subject1=models.CharField(max_length=15)
    subject2=models.CharField(max_length=15)
    subject3=models.CharField(max_length=15)
    subject4=models.CharField(max_length=15)
    subject5=models.CharField(max_length=15)
    subject6=models.CharField(max_length=15)

# class grade(sections,subjects):
#     gradeName= models.CharField(max_length=3)
#     #sections= models.ForeignKey(sections,on_delete=models.CASCADE,default='')
#     gid= models.AutoField(primary_key=True)
#     # subject1=models.CharField(max_length=15)
#     # subject2=models.CharField(max_length=15)
#     # subject3=models.CharField(max_length=15)
#     # subject4=models.CharField(max_length=15)
#     # subject5=models.CharField(max_length=15)
#     # subject6=models.CharField(max_length=15)
