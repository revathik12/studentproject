from django.contrib import admin
from grade.models import sections,subjects

# Register your models here.
# class AdminGrade(admin.ModelAdmin):
#     list_display=['gradeName']

# class AdminSection(admin.ModelAdmin):
#     list_display=['section']

# class AdminSubject(admin.ModelAdmin):
#     list_display=['subject1','subject2','subject3','subject4','subject5','subject6']

#admin.site.register(grade)
admin.site.register(sections)
admin.site.register(subjects)